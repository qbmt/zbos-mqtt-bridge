"""Integrates Home Assistant with ZBOS (Zorabots Operating System)."""
import json
import logging

import voluptuous as vol

from homeassistant.const import (EVENT_STATE_CHANGED, MATCH_ALL)
from homeassistant.core import EventOrigin
from homeassistant.exceptions import (
    ServiceNotFound)
from homeassistant.helpers.json import JSONEncoder
from homeassistant.helpers.service import async_get_all_descriptions

_LOGGER = logging.getLogger(__name__)
DOMAIN = 'zbos'

# Listen topics
ZBOS_HASS_GET_STATES = 'zbos/hass/get/states'
ZBOS_HASS_GET_STATE = 'zbos/hass/get/state'
# ZBOS_HASS_SET_STATE = 'zbos/hass/set/state'  # Not needed for now?
ZBOS_HASS_GET_SERVICES = 'zbos/hass/get/services'
ZBOS_HASS_GET_EVENTS = 'zbos/hass/get/events'
ZBOS_HASS_GET_CONFIG = 'zbos/hass/get/config'
ZBOS_HASS_GET_COMPONENTS = 'zbos/hass/get/components'
ZBOS_HASS_CALL_SERVICE = 'zbos/hass/call_service'

# Publish topics
ZBOS_HASS_EVENT = 'zbos/hass/event'
ZBOS_HASS_RESPONSE = 'zbos/hass/response/{}'


async def async_setup(hass, config):
    mqtt = hass.components.mqtt
    target_events = [
        EVENT_STATE_CHANGED,
    ]

    async def _event_publisher(event):
        """Publish hass events on the MQTT queue."""
        if event.origin != EventOrigin.local:
            return
        if event.event_type not in target_events:
            return

        msg = json.dumps(event, cls=JSONEncoder)
        mqtt.async_publish(ZBOS_HASS_EVENT, msg)

    hass.bus.async_listen(MATCH_ALL, _event_publisher)

    # Process events from a remote server that are received on a queue.
    async def _event_receiver(msg):
        """Receive mqtt events."""
        # Get all states.
        if msg.topic == ZBOS_HASS_GET_STATES:
            event = json.loads(msg.payload)
            event_key = event.get('key')

            states = []
            if event_key:
                for state in hass.states.async_all():
                    states.append(state.as_dict())

                response = json.dumps(states, cls=JSONEncoder)
                mqtt.async_publish(ZBOS_HASS_RESPONSE.format(event_key), response)

        # Get a specific state.
        elif msg.topic == ZBOS_HASS_GET_STATE:
            event = json.loads(msg.payload)
            entity_id = event.get('entity_id')
            event_key = event.get('key')

            state = hass.states.get(entity_id)
            if state:
                response = json.dumps(state, cls=JSONEncoder)
                mqtt.async_publish(ZBOS_HASS_RESPONSE.format(event_key), response)
            else:
                error = {"error": {'code': 'no_state', 'text': "Entity not found."}}
                mqtt.async_publish(ZBOS_HASS_RESPONSE.format(event_key), error)

        # Call a service.
        elif msg.topic == ZBOS_HASS_CALL_SERVICE:
            event = json.loads(msg.payload)
            domain = event.get('domain')
            service = event.get('service')
            data = event.get('data')
            try:
                await hass.services.async_call(domain, service, data, True)
            except (vol.Invalid, ServiceNotFound):
                pass

        # Get all services.
        elif msg.topic == ZBOS_HASS_GET_SERVICES:
            event = json.loads(msg.payload)
            event_key = event.get('key')
            descriptions = async_get_all_descriptions(hass)
            services = [{'domain': key, 'services': value} for key, value in descriptions.items()]
            response = json.dumps(services, cls=JSONEncoder)
            mqtt.async_publish(ZBOS_HASS_RESPONSE.format(event_key), response)

        # Get all events.
        elif msg.topic == ZBOS_HASS_GET_EVENTS:
            event = json.loads(msg.payload)
            event_key = event.get('key')
            events = [{'event': key, 'listener_count': value} for key, value in hass.bus.async_listeners().items()]
            response = json.dumps(events, cls=JSONEncoder)
            mqtt.async_publish(ZBOS_HASS_RESPONSE.format(event_key), response)

        # Get current config.
        elif msg.topic == ZBOS_HASS_GET_CONFIG:
            event = json.loads(msg.payload)
            event_key = event.get('key')
            response = json.dumps(hass.config.as_dict(), cls=JSONEncoder)
            mqtt.async_publish(ZBOS_HASS_RESPONSE.format(event_key), response)

        # Get current components.
        elif msg.topic == ZBOS_HASS_GET_COMPONENTS:
            event = json.loads(msg.payload)
            event_key = event.get('key')
            response = json.dumps(hass.config.components, cls=JSONEncoder)
            mqtt.async_publish(ZBOS_HASS_RESPONSE.format(event_key), response)

    # Subscribes.
    await mqtt.async_subscribe(ZBOS_HASS_GET_STATES, _event_receiver)
    await mqtt.async_subscribe(ZBOS_HASS_GET_STATE, _event_receiver)
    await mqtt.async_subscribe(ZBOS_HASS_CALL_SERVICE, _event_receiver)
    await mqtt.async_subscribe(ZBOS_HASS_GET_SERVICES, _event_receiver)
    await mqtt.async_subscribe(ZBOS_HASS_GET_EVENTS, _event_receiver)
    await mqtt.async_subscribe(ZBOS_HASS_GET_CONFIG, _event_receiver)
    await mqtt.async_subscribe(ZBOS_HASS_GET_COMPONENTS, _event_receiver)

    return True
