FROM python:3-alpine
LABEL maintainer="Zorabots <beheerder@qbmt.be>"

RUN apk update && \
    apk upgrade

WORKDIR /app
COPY zbos-mqtt-bridge/mqttbridge.py /app/

EXPOSE 3218
VOLUME /config

ENV HC_GIT true
ENV HC_BASEPATH /config

ENTRYPOINT ["python", "/app/mqttbridge.py"]